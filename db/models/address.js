'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Address.belongsTo(models.Customers);
    }
  }
  Address.init({
    UfParameter: DataTypes.STRING,
    Logradouro: DataTypes.STRING,
    Numero: DataTypes.STRING,
    Complemento: DataTypes.STRING,
    CEP: DataTypes.STRING,
    Bairro: DataTypes.STRING,
    Municipio: DataTypes.STRING,
    Uf: DataTypes.STRING,
    CodeIbge: DataTypes.STRING,
    CustomersId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Address',
    timestamps: false,
    freezeTableName: true,
  });
  return Address;
};