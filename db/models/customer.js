'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Address, { foreignKey: "CustomersId" });
      this.hasMany(models.Card, { foreignKey: "CustomersId" });
    }
  }
  Customer.init({
    CodeHtml: DataTypes.STRING,
    CodeInternal: DataTypes.STRING,
    CnpjParameter: DataTypes.STRING,
    CNPJConsulted: DataTypes.STRING,
    NumberRegistration: DataTypes.STRING,
    NameBusiness: DataTypes.STRING,
    RegistrationStage: DataTypes.STRING,
    SupplierName: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Customers',
    timestamps: false,
    freezeTableName: true,
  });
  return Customer;
};