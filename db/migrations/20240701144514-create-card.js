'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Card', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      NumberCard: { type: Sequelize.STRING },
      ExpirationDate: { type: Sequelize.DATE },
      NameCard: { type: Sequelize.STRING },
      Band: { type: Sequelize.STRING },
      SecurityCode: { type: Sequelize.STRING },
      CustomersId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Customers',
          key: 'Id'
        }
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Card');
  }
};